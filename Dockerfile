FROM amazoncorretto:17.0.8
MAINTAINER zidanetri
COPY target/OpenShiftSample-0.0.1-SNAPSHOT.jar OpenShiftSample.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/OpenShiftSample.jar"]
