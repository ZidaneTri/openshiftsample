package com.innowise.openshiftsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenShiftSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenShiftSampleApplication.class, args);
    }

}
