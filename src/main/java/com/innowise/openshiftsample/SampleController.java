package com.innowise.openshiftsample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping("/sample")
    public String samplePath(){
        return "Sample Working Successfully";
    }

}
